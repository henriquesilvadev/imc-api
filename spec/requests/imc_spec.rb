require 'rails_helper'

RSpec.describe "Imcs", type: :request do

  describe "POST /imc" do
    let(:imc_result) {JSON(body)["imc"]}
    let(:classification) {JSON(body)["classification"]}
  
    let(:thinness) {{ height: 1.75, weight: 56 }}
    let(:overweight) {{ height: 1.70, weight: 76 }}
    let(:grade_2_obesity) {{ height: 1.60, weight: 96 }}

    it "must return with the imc equal to '18.29' and the classification of 'Thinness'" do
      post "/imc", params: thinness
      
      expect(imc_result).to eq(18.29)
      expect(classification).to eq("Thinness")
      expect(response).to have_http_status(:success)
    end
    
    it "must return with the imc equal to '26.3' and the classification of 'Overweight'" do
      post "/imc", params: overweight
      
      expect(imc_result).to eq(26.3)
      expect(classification).to eq("Overweight")
      expect(response).to have_http_status(:success)
    end

    it "must return with the imc equal to '37.5' and the classification of 'Grade II obesity'" do
      post "/imc", params: grade_2_obesity

      expect(imc_result).to eq(37.5)
      expect(classification).to eq("Grade II obesity")
      expect(response).to have_http_status(:success)
    end
  end

end

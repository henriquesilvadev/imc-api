class ImcController < Api::BaseController

  before_action :height, :weight, only: :imc
  
  def imc
    @imc = calculate_imc(@height, @weight)
    render json: @imc
  end

  private

  def calculate_imc(height,weight)
    @imc_result = weight/(height * height)
    
    if(@imc_result < 18.5)   
      @classification = 'Thinness'    
    elsif((@imc_result > 18.5) && (@imc_result < 24.9))
      @classification = 'Normal weight'    
    elsif((@imc_result > 24.9) && (@imc_result < 30))  
      @classification = 'Overweight'    
    elsif((@imc_result > 30) && (@imc_result < 34.9))
      @classification = 'Grade I obesity'   
    elsif((@imc_result > 34.9) && (@imc_result < 39.9))
      @classification = 'Grade II obesity'  
    end
    { imc: @imc_result.round(2), classification: @classification }
  
  end

  def imc_params
    params.permit(:height, :weight)
  end

  def height
    @height = imc_params[:height].to_f
  end

  def weight
    @weight = imc_params[:weight].to_f
  end
end

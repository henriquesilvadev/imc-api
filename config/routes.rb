Rails.application.routes.draw do

  devise_for :users,
  defaults: { format: :json },
  path: '',
  path_names: {
    sign_out: 'api/logout',
    registration: 'api/signup'
  },
  controllers: {
    sessions: 'sessions',
    registrations: 'registrations'
  }
  
  post "/imc" => "imc#imc", :as => :imc
end

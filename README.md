# IMC-API

# docker-compose - Rails 6.0 e Postgres
Docker compose com rails e postgres

## Clone this
Open your app directory in terminal

Clone this repositore in your directory

```$ git clone https://gitlab.com/henriquesilvadev/imc-api.git .```

## Build Dockerfile
```$ docker-compose build```

## Create rails app
```$ docker-compose up -d```


## Configure your database
```$ docker-compose exec web rails db:create db:migrate ```

```$ sudo chown -R $USER:$USER .```

## Run Rspec
```$ docker-compose exec web rspec ```